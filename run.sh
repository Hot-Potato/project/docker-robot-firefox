#!/bin/bash
# Entry script to start Xvfb and set display
set -e

# Give docker-compose up time to start the web server
sleep 15

# Set the defaults
DEFAULT_LOG_LEVEL="INFO" # Available levels: TRACE, DEBUG, INFO (default), WARN, NONE (no logging)
DEFAULT_RES="1920x1080x24"
DEFAULT_DISPLAY=":99"
DEFAULT_ROBOT_TESTS="false"

# Use default if none specified as env var
LOG_LEVEL=${LOG_LEVEL:-$DEFAULT_LOG_LEVEL}
RES=${RES:-$DEFAULT_RES}
DISPLAY=${DISPLAY:-$DEFAULT_DISPLAY}
ROBOT_TESTS=${ROBOT_TESTS:-$ROBOT_TESTS}

function cleanup {
  killall Xvfb
}

trap cleanup EXIT

# Start Xvfb
echo -e "Starting Xvfb on display ${DISPLAY} with res ${RES}"
Xvfb ${DISPLAY} -ac -screen 0 ${RES} +extension RANDR &
export DISPLAY=${DISPLAY}

cp /repo/geckodriver /usr/local/sbin/

DISPLAY=:99 robot -d /repo/output/ /repo/tests/*.robot
