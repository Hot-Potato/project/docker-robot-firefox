Manually building for testing:

docker build -t robot-firefox .

Run it without any mounts to Google search for the Catalyst Website:

docker run -it robot-firefox

Run it with a few mounts to test other things:

docker run -it -v $(pwd)/output/:/repo/output/ -v $(pwd)/tests/:/repo/tests/ robot-firefox

# Notes

http://robotframework.org/

http://robotframework.org/#tools

https://github.com/robotframework/robotframework/blob/master/INSTALL.rst

https://github.com/robotframework/QuickStartGuide/blob/master/QuickStart.rst

https://www.slideshare.net/pekkaklarck/robot-framework-dos-and-donts

https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst
