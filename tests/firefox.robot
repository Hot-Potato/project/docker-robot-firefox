*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${LOGIN URL}      https://google.co.nz/
${BROWSER}        Firefox

*** Test Cases ***
Google the Catalyst IT website
    Open Browser To Google NZ
    Input Search    Catalyst IT
    Search Google
    View First Result
    Capture Page Screenshot
    [Teardown]    Close Browser

*** Keywords ***
Open Browser To Google NZ
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Title Should Be    Google

Input Search
    [Arguments]    ${searchtext}
    Input Text    q    ${searchtext}

Search Google
    Click Button    btnI

View First Result
    Title Should Be    Home | Catalyst
    Page Should Contain   Freedom to innovate

