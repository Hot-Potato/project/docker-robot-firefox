FROM ubuntu:bionic


RUN apt-get update && apt-get upgrade -y && apt-get install -y firefox python-pip xvfb gcc dbus psmisc socat && pip install robotframework robotframework-selenium2library selenium robotframework-xvfb && pip install --upgrade robotframework-seleniumlibrary && apt-get remove -y gcc --purge && apt-get clean && rm -rf /var/lib/apt/lists/*

#COPY policies.json /usr/lib/firefox/distribution/
COPY firefox-mozilla-build_75.0-0ubuntu1_amd64.deb /

RUN dpkg -i firefox-mozilla-build_75.0-0ubuntu1_amd64.deb

COPY . /repo/

RUN chmod +x /repo/run.sh

CMD ["/repo/run.sh"]
